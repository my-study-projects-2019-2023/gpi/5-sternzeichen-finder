﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace _5_SternzeichenFinder
{
    [TestClass]
    public class FindeSternzeichenZuGeburtstagTest
    {
        [TestMethod]
        public void SternzeichenZu05T1M_ShouldBeSteinbock()
        {
            int inputMonat = 1;
            int inputTag = 5;
            string actualSternzeichen = Sternzeichen.GetSternzeichenOf(inputMonat, inputTag);
            actualSternzeichen.Should().Be("Steinbock");
        }

        [TestMethod]
        public void SternzeichenZu20T12M_ShouldBeSteinbock()
        {
            int inputMonat = 12;
            int inputTag = 23;
            string actualSternzeichen = Sternzeichen.GetSternzeichenOf(inputMonat, inputTag);
            actualSternzeichen.Should().Be("Steinbock");
        }

        [TestMethod]
        public void SternzeichenZu21T2M_ShouldBeWassermann()
        {
            int inputMonat = 1;
            int inputTag = 21;
            string actualSternzeichen = Sternzeichen.GetSternzeichenOf(inputMonat, inputTag);
            actualSternzeichen.Should().Be("Wassermann");
        }

        [TestMethod]
        public void SternzeichenZu11T3M_ShouldBeFische()
        {
            int inputMonat = 3;
            int inputTag = 11;
            string actualSternzeichen = Sternzeichen.GetSternzeichenOf(inputMonat, inputTag);
            actualSternzeichen.Should().Be("Fische");
        }

        [TestMethod]
        public void SternzeichenZu12T2M_ShouldBeFische()
        {
            int inputMonat = 2;
            int inputTag = 20;
            string actualSternzeichen = Sternzeichen.GetSternzeichenOf(inputMonat, inputTag);
            actualSternzeichen.Should().Be("Fische");
        }

        [TestMethod]
        public void SternzeichenZu22T5M_ShouldBeZwilling()
        {
            int inputMonat = 5;
            int inputTag = 22;
            string actualSternzeichen = Sternzeichen.GetSternzeichenOf(inputMonat, inputTag);
            actualSternzeichen.Should().Be("Zwilling");
        }


    }
}
