﻿using _5_SternzeichenFinder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SternzeichenFinder
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bitte gebe deinen Geburtsmonat und deinen Geburtstag ein (Format TT.MM, also z.B. 01.01):");
            string geburtstag = Console.ReadLine();
            IEnumerable<string> geburtstagsBestandteile = geburtstag.Split('.');
            int geburtstagsTag = 0;
            int geburtstagsMonat = 0;
             if(Int32.TryParse(geburtstagsBestandteile?.ToList()?[0], out geburtstagsTag) && geburtstagsTag > 0 && geburtstagsTag < 31 &&
                Int32.TryParse(geburtstagsBestandteile?.ToList()?[1], out geburtstagsMonat) && geburtstagsMonat > 0 && geburtstagsMonat < 13)
             {
                Console.WriteLine("Dein Sternzeichen ist: " + Sternzeichen.GetSternzeichenOf(geburtstagsMonat, geburtstagsTag));
             }
             else
            {
                Console.WriteLine("Deine Eingabe war leider nicht in dem korreten Format.");
            }
            Console.ReadKey();
        }
    }
}
