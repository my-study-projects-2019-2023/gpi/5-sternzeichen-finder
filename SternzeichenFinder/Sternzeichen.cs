﻿using System.Collections.Generic;
using System.Linq;

namespace _5_SternzeichenFinder
{
    public class Sternzeichen
    {
        private int startMonat;
        private int startTag;

        private int endMonat;
        private int endTag;

        private string sternzeichenBezeichnung;

        public Sternzeichen(int startTag, int startMonat, int endTag, int endMonat, string sternzeichenBezeichnung)
        {
            this.startMonat = startMonat;
            this.startTag = startTag;
            this.endMonat = endMonat;
            this.endTag = endTag;
            this.sternzeichenBezeichnung = sternzeichenBezeichnung;
        }

        public int StartMonat { get => startMonat; set => startMonat = value; }
        public int StartTag { get => startTag; set => startTag = value; }
        public int EndMonat { get => endMonat; set => endMonat = value; }
        public int EndTag { get => endTag; set => endTag = value; }
        public string SternzeichenBezeichnung { get => sternzeichenBezeichnung; set => sternzeichenBezeichnung = value; }


        public static string GetSternzeichenOf(int inputMonat, int inputTag)
        {
            List<Sternzeichen> sternzeichen = new List<Sternzeichen>();
            sternzeichen.Add(new Sternzeichen(22, 12, 19, 1, "Steinbock"));
            sternzeichen.Add(new Sternzeichen(20, 1, 19, 2, "Wassermann"));
            sternzeichen.Add(new Sternzeichen(20, 2, 20, 3, "Fische"));
            sternzeichen.Add(new Sternzeichen(21, 3, 19, 4, "Widder"));
            sternzeichen.Add(new Sternzeichen(20, 4, 20, 5, "Stier"));
            sternzeichen.Add(new Sternzeichen(21, 5, 20, 6, "Zwilling"));
            sternzeichen.Add(new Sternzeichen(21, 6, 21, 7, "Krebs"));
            sternzeichen.Add(new Sternzeichen(22, 7, 22, 8, "Löwe"));
            sternzeichen.Add(new Sternzeichen(23, 8, 22, 9, "Jungfrau"));
            sternzeichen.Add(new Sternzeichen(23, 9, 22, 10, "Waage"));
            sternzeichen.Add(new Sternzeichen(23, 10, 21, 11, "Skorpion"));
            sternzeichen.Add(new Sternzeichen(22, 11, 22, 12, "Schütze"));


            return sternzeichen.FirstOrDefault(x => {
            if(
                ((inputMonat == x.StartMonat && inputTag >= x.StartTag) &&
                (inputMonat != x.EndMonat)) ||
                ((inputMonat != x.StartMonat) &&
                (inputMonat == x.EndMonat && inputTag <= x.EndTag))) 
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            ).SternzeichenBezeichnung;

        }
    }
}